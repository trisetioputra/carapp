package com.example.carapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View


class MainActivity : AppCompatActivity() {
    private lateinit var rvCar: RecyclerView
    private var list: ArrayList<Car> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvCar = findViewById(R.id.rv_cars)
        rvCar.setHasFixedSize(true)
        list.addAll(CarData.listData)
        showRecyclerList()
    }
    private fun showRecyclerList() {
        rvCar.layoutManager = LinearLayoutManager(this)
        val listCarAdapter = ListCarAdapter(list)
        rvCar.adapter = listCarAdapter
        listCarAdapter.setOnItemClickCallback(object : ListCarAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Car) {
                val moveIntent = Intent(this@MainActivity, CarDetail::class.java)
                moveIntent.putExtra(CarDetail.IDMOBIL,data.carID)
                startActivity(moveIntent)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val prof:MenuInflater=menuInflater
        prof.inflate(R.menu.actionbar1,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId==R.id.profile){
            val moveIntent= Intent(this@MainActivity, Profile::class.java)
            startActivity(moveIntent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

//private fun showRecyclerList() {
//    rvCar.layoutManager = LinearLayoutManager(this)
//    val listCarAdapter = ListCarAdapter(list)
//    rvCar.adapter = listCarAdapter
//    listCarAdapter.setOnItemClickCallback(object : ListCarAdapter.OnItemClickCallback {
//        override fun onItemClicked(data: Car) {
//            val moveIntent = Intent(this@MainActivity, CarDetail::class.java)
//            moveIntent.putExtra(moveIntent.id, data.carName)
//            startActivity(moveIntent)
//        }
//    })
//}