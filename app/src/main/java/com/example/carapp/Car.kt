package com.example.carapp

data class Car (
    var carID:Int=0,
    var carName : String="",
    var detail: String="",
    var ccMesin: String="",
    var kapasitas :String="",
    var transmisi:String="",
    var kelas: String="",
    var kondisi: String="",
    var tahun:Int=0,
    var photo: Int=0,
    var harga: Int=0,
    var isFavorite:Boolean=false
    )