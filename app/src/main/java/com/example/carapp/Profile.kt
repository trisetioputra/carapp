package com.example.carapp

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Profile: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)
        var favorit: TextView =findViewById(R.id.mobil_Fav)
        var temp:String=""
        var counter=0
        for(indeks in CarData.listData.indices){
            if(CarData.listData[indeks].isFavorite){
                counter+=1
                temp+=counter.toString()+". "+CarData.listData[indeks].carName+"\n"
            }
        }
        if(temp==""){
            favorit.text="None"
        }
        favorit.text=temp
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }
}