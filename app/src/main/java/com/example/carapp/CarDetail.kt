package com.example.carapp
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class CarDetail:AppCompatActivity(), View.OnClickListener{
    private var IdMobil: Int = 0

    companion object{
        const val IDMOBIL = "name"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.car_detail)
        var tvName: TextView = findViewById(R.id.item_nama)
        var tvKelas: TextView = findViewById(R.id.item_kelas)
        var tvharga: TextView= findViewById(R.id.item_harga)
        var imgPhoto: ImageView = findViewById(R.id.img_item_photo)
        var tvdetail: TextView = findViewById(R.id.item_deskripsi)
        var tvkapasitas:TextView=findViewById(R.id.kapasitas)
        var tvkondisi:TextView=findViewById(R.id.kondisi)
        var tvtahun:TextView=findViewById(R.id.item_tahun)
        var tvccmesin:TextView=findViewById(R.id.item_ccmesin)
        var tvtransmisi:TextView=findViewById(R.id.transmisi)


        var idmobil=intent.getIntExtra(IDMOBIL,0)-1
        IdMobil=idmobil
        tvName.text=CarData.listData[idmobil].carName
        tvKelas.text=CarData.listData[idmobil].kelas
        tvharga.text="RP."+CarData.listData[idmobil].harga.toString()+" rb/hari"
        tvdetail.text=CarData.listData[idmobil].detail
        tvccmesin.text=CarData.listData[idmobil].ccMesin
        tvkondisi.text=CarData.listData[idmobil].kondisi
        tvtransmisi.text=CarData.listData[idmobil].transmisi

        Glide.with(this)
            .load(CarData.listData[idmobil].photo)
            .apply(RequestOptions().override(550, 550))
            .into(imgPhoto)
        tvkapasitas.text=CarData.listData[idmobil].kapasitas
        tvtahun.text=CarData.listData[idmobil].tahun.toString()

        var tombol:Button=findViewById(R.id.btn_fav)
        tombol.setOnClickListener(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if(CarData.isFavorite[this.IdMobil]) {
            tombol.text="HAPUS DARI FAVORIT"
        }
        else{
            tombol.text="TAMBAH KE FAVORIT"
        }

    }
    override fun onClick(v: View?) {
        if (v?.id == R.id.btn_fav) {
            CarData.isFavorite[this.IdMobil] = !CarData.isFavorite[this.IdMobil]
            Toast.makeText(this, "status menjadi "+ CarData.isFavorite[this.IdMobil].toString(), Toast.LENGTH_SHORT).show()

            val moveIntent= Intent(this@CarDetail, MainActivity::class.java)
            startActivity(moveIntent)
        }

    }
}