package com.example.carapp

object CarData {
    private val carID = arrayOf(1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10)
    private val carName = arrayOf(
        "Toyota Avanza",
        "Daihatsu Ayla",
        "Daihatsu Sigra",
        "Honda HRV",
        "Honda Jazz",
        "Honda Mobilio",
        "Toyota Innova",
        "Toyota Raize",
        "Toyota Yaris",
        "Wuling Almaz"
    )

    private val detail = arrayOf("Mobil keluarga paling nyaman pilihan keluarga Indonesia. Nikmati kesenyapan dan kenyamanan berkendara terbaik besama keluarga. (src:toyota.astra.co.id)",
        "Sejak 2012, mobil Ayla merupakan salah satu pelopor LCGC (Low Cost and Green Car) buatan Daihatsu. Pertama kali diperkenalkan ke publik, Ayla sukses menarik perhatian masyarakat bahkan total penjualannya tercatat 180 ribu unit kendaraan di tahun pertama rilis. (src:daihatsu.co.id)",
        "7 kursi Mini MPV yang dirancang oleh Daihatsu dan diproduksi oleh Astra Daihatsu Motor di Indonesia sejak 2016 untuk dijual secara eksklusif di negara ini. Dibangun di atas platform Ayla yang lebih panjang dan diposisikan di bawah Xenia sebagai pilihan MPV yang lebih terjangkau untuk pasar Indonesia, di mana permintaan akan MPV 3-baris tinggi. (src:daihatsu.co.id)",
        "Honda HR-V adalah sebuah mobil SUV mini yang diproduksi oleh Honda pada tahun 1998 sampai 2006 lalu dari tahun 2014 sampai sekarang.HR-V diperkenalkan untuk memenuhi permintaan untuk kendaraan dengan manfaat seperti SUV (khususnya untuk model yang lebih besar seperti CR-V), seperti peningkatan ruang kargo dan visibilitas yang lebih tinggi, bersama dengan manuver, kinerja dan konsumsi bahan bakar dari mobil yang lebih kecil. src(wikipedia.org)",
        "Honda Jazz adalah mobil hatchback 5 pintu produksi pabrikan otomotif Jepang Honda Motor Company. Honda Jazz pertama kali diperkenalkan pada 2001. Nama Honda Jazz digunakan di Eropa, sebagian Asia, Australia, Oceania, Timur Tengah, dan Afrika. Di Jepang, China, dan Amerika, mobil ini dinamai Honda Fit. Sampai tahun 2007, telah terjual 2 juta unit Honda Jazz di seluruh dunia. (src:wikipedia.org)",
        "Mobilio adalah mobil keluarga tujuh penumpang yang diproduksi pabrikan mobil asal Jepang, Mobil Honda. Generasi pertama Mobilio yang diproduksi tahun 2001-2008, merupakan seri kedua dari Honda Small Max. Mobil ini dibangun dari konsep Honda’s Global Small Platform dengan mesin i-DSI. (src:wikipedia.org)",
        "Pada tahun 2004, Toyota meluncurkan jenis Kijang Innova untuk pertama kalinya dan termasuk ke dalam mobil jenis Multi Purpose Vehicle (MPV). Saat ini, model Kijang Innova terlihat lebih modern dan memiliki fitur-fitur canggih baik dari sisi eksterior maupun interiornya.Ukurannya memang besar, namun cukup ringan dikendarai. Sehingga sangat ideal untuk perjalanan jauh bersama keluarga. (src:otomotif.antara.news)",
        "PT Toyota Astra Motor (TAM) telah memperkenalkan lini segmen compact Sport Utility Vehicle (SUV) lima penumpang terbaru yakni Toyota Raize. Toyota Raize hadir dalam rangka menyambut 50 tahun eksistensi Toyota di Indonesia sekaligus memenuhi kebutuhan pelanggan terhadap kendaraan segmen SUV yang terus meningkat. (src:auto2000)",
        "Toyota Yaris adalah hatchback produksi Toyota yang cukup populer karena desainnya yang membulat dan unik. Mobil ini pertama kali diluncurkan di Indonesia pada tahun 2006 dan mencoba bersaing dengan hatchback produksi Honda, yaitu Jazz yang sangat populer pada saat itu. Toyota Yaris juga dibekali mesin yang sama dengan Toyota Vios, yaitu 1.5L 1NZ-FE DOHC VVT-i yang dikenal bandel dan irit. (src:otoloka.id) ",
        "Wuling Almaz tersedia dalam pilihan mesin Bensin di Indonesia SUV baru dari Wuling hadir dalam 8 varian. Bicara soal spesifikasi mesin Wuling Almaz, ini ditenagai dua pilihan mesin Bensin berkapasitas 1451 cc. (src:Oto.com)")
    private val ccMesin = arrayOf("1300cc-1500cc",
        "1000cc-1300cc",
        "1000cc-1300cc",
        "1500cc-1800cc",
        "1500cc",
        "1500cc",
        "2000cc-2400cc",
        "1000cc-1200cc",
        "1500cc",
        "2000cc")
    private val kapasitas = arrayOf("7 orang",
        "4 orang",
        "7 orang",
        "5 orang",
        "5 orang",
        "7 orang",
        "7 orang",
        "5 orang",
        "5 orang",
        "5-7 orang")
    private val kelas = arrayOf("Low MPV",
        "LCGC Hatchback",
        "LCGC MPV",
        "Crossover",
        "Hatchback",
        "low MPV",
        "Medium MPV",
        "Small SUV",
        "Hatchback",
        "Medium SUV")
    private val transmisi = arrayOf("Manual",
        "Manual",
        "Automatic",
        "Automatic",
        "Automatic",
        "Automatic",
        "Manual/Automatic",
        "Automatic",
        "Automatic",
        "Triptonic")
    private val kondisi = arrayOf("Tersedia",
        "Tersedia",
        "Tersedia",
        "Booked",
        "Booked",
        "Tersedia",
        "Booked",
        "Booked",
        "Tersedia",
        "Booked")
    private val photo = intArrayOf(R.drawable.avanza,
        R.drawable.ayla,
        R.drawable.daihatsu_png,
        R.drawable.honda_hrv,
        R.drawable.honda_jazz,
        R.drawable.honda_mobilio,
        R.drawable.innova,
        R.drawable.toyota_raize_2,
        R.drawable.toyota_yaris,
        R.drawable.wuling_almaz)
    private val harga = intArrayOf(100,
        110,
        80,
        250,
        200,
        180,
        350,
        250,
        200,
        280)
    private val tahun = intArrayOf(2016,
        2017,
        2017,
        2019,
        2018,
        2016,
        2016,
        2020,
        2019,
        2020)
    val isFavorite = booleanArrayOf(false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false)

    //favorite sudah auto false
    val listData: ArrayList<Car>
        get() {
            val list = arrayListOf<Car>()
            for (position in carName.indices) {
                val car = Car()
                car.carID= carID[position]
                car.carName = carName[position]
                car.detail = detail[position]
                car.ccMesin = ccMesin[position]
                car.harga= harga[position]
                car.photo= photo[position]
                car.kelas= kelas[position]
                car.transmisi= transmisi[position]
                car.kondisi= kondisi[position]
                car.kapasitas= kapasitas[position]
                car.tahun= tahun[position]
                car.isFavorite= isFavorite[position]
                list.add(car)
            }
            return list
        }

}