package com.example.carapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ListCarAdapter(private val listCar: ArrayList<Car>) : RecyclerView.Adapter<ListCarAdapter.ListViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback

    interface OnItemClickCallback {
        fun onItemClicked(data: Car)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_mobil, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListCarAdapter.ListViewHolder, position: Int) {
        val car = listCar[position]
        Glide.with(holder.itemView.context)
            .load(car.photo)
            .apply(RequestOptions().override(550, 550))
            .into(holder.imgPhoto)
        holder.tvName.text = car.carName.toUpperCase()
        holder.tvKelas.text = car.kelas
        holder.tvharga.text = "RP."+car.harga.toString()+" Rb/hari"

        holder.itemView.setOnClickListener { onItemClickCallback.onItemClicked(listCar[holder.adapterPosition]) }
    }

    override fun getItemCount(): Int {
        return listCar.size
    }
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }
    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.item_nama)
        var tvKelas: TextView = itemView.findViewById(R.id.item_kelas)
        var tvharga: TextView= itemView.findViewById(R.id.item_harga)
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_item_photo)
    }

}